# remove qtwebkit
RDEPENDS_${PN}_remove = "qtwebkit-dev qtwebkit-mkspecs qtwebkit-qmlplugins"
RDEPENDS_${PN}_remove = "qtcharts-dev qtcharts-mkspecs qtcharts-qmlplugins"

# remove qtquick
RDEPENDS_${PN}_remove = "qtquick1-dev qtquick1-mkspecs qtquick1-plugins qtquick1-qmlplugins qttranslations-qtquick1 qttranslations-qt"

# --> sdk for a minimal qt image
# remove qtcharts 
RDEPENDS_${PN}_remove = "qtcharts-dev qtcharts-mkspecs"

# remove some qttools stuff
RDEPENDS_${PN}_remove = "qttools-mkspecs qttools-staticdev"
# <-- sdk for a minimal qt image
