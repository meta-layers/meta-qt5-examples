# Things we don't use:
# PACKAGECONFIG_DEFAULT="accessibility udev evdev widgets tools libs freetype tests pcre"
PACKAGECONFIG_DEFAULT_remove = "accessibility"
# bluetooth depends on dbus, so I disabled bluetooth in DISTRO_FEATURES
PACKAGECONFIG_DEFAULT_remove = "dbus"
PACKAGECONFIG_DEFAULT_remove = "tests"
PACKAGECONFIG_DEFAULT_remove = "widgets"

# PACKAGECONFIG="release accessibility udev evdev tools libs freetype  pcre openssl no-opengl jpeg libpng zlib"
# we could remove a few more here
PACKAGECONFIG_remove = "accessibility"
