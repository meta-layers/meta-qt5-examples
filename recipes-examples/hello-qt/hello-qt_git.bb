# Copyright (C) 2020 Robert Berger <robert.berger@ReliableEmbeddedSystems.com>
# Released under the MIT license (see COPYING.MIT for the terms)

LICENSE = "GPL-2.0"
LIC_FILES_CHKSUM = "file://LICENSE;md5=d7ae45e19108e890d6501374320fc40f"

SRC_URI = "git://gitlab.com/exempli-gratia/hello-qt-sdk.git;protocol=https"

# Modify these as desired
PV = "1.0+git${SRCPV}"
#SRCREV = "170daf4616723edb96de0c8fd15b6dff5432e173"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git/src"

DEPENDS = "qtbase"

# cmake_qt5 inherits cmake
inherit cmake_qt5

# Specify any options you want to pass to cmake using EXTRA_OECMAKE:
EXTRA_OECMAKE = ""

# not actually needed here, 
# since we already include qtbase in image recipe 
RDEPENDS_${PN} = "qtbase"
